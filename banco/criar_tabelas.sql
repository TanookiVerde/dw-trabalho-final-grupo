create table if not exists dim_data
(
	id int auto_increment
		primary key,
	datetime timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP
);

create table if not exists dim_localizacao
(
	id int auto_increment
		primary key,
	population bigint(50) null,
	population_density decimal(20) null,
	median_age decimal(20) null,
	aged_65_older decimal(20) null,
	aged_70_older decimal(20) null,
	gdp_per_capita decimal(20) null,
	extreme_poverty decimal(20) null,
	stringency_index decimal(20) null,
	cardiovasc_death_rate decimal(20) null,
	diabetes_prevalence decimal(20) null,
	female_smokers decimal(20) null,
	male_smokers decimal(20) null,
	life_expectancy decimal(20) null,
	handwashing_facilities decimal(20) null,
	hospital_beds_per_thousand decimal(20) null,
	human_development_index decimal(20) null
);

create table if not exists dim_regiao_geografica
(
	id int auto_increment
		primary key,
	cod_iso varchar(10) null,
	ds_location varchar(50) null,
	ds_continent varchar(30) null
);

create table if not exists dim_testes
(
	id int auto_increment
		primary key,
	tests_unit varchar(50) not null
);

create table if not exists fato_observacao
(
	id int auto_increment
		primary key,
	fk_testes int null,
	fk_localizacao int null,
	fk_data int null,
	fk_regiao_geografica int null,
	total_cases decimal(20) null,
	new_cases decimal(20) null,
	new_cases_smoothed decimal(20) null,
	total_deaths decimal(20) null,
	new_deaths decimal(20) null,
	new_deaths_smoothed decimal(20) null,
	total_cases_per_million decimal(20) null,
	new_cases_per_million decimal(20) null,
	new_cases_smoothed_per_million decimal(20) null,
	total_deaths_per_million decimal(20) null,
	new_deaths_per_million decimal(20) null,
	new_deaths_smoothed_per_million decimal(20) null,
	reproduction_rate decimal(20) null,
	icu_patients decimal(20) null,
	icu_patients_per_million decimal(20) null,
	hosp_patients decimal(20) null,
	hosp_patients_per_million decimal(20) null,
	weekly_icu_admissions decimal(20) null,
	weekly_icu_admissions_per_million decimal(20) null,
	weekly_hosp_admissions decimal(20) null,
	weekly_hosp_admissions_per_million decimal(20) null,
	new_tests decimal(20) null,
	total_tests decimal(20) null,
	total_tests_per_thousand decimal(20) null,
	new_tests_per_thousand decimal(20) null,
	new_tests_smoothed decimal(20) null,
	new_tests_smoothed_per_thousand decimal(20) null,
	positive_rate decimal(20) null,
	tests_per_case decimal(20) null,
	total_vaccinations decimal(20) null,
	people_vaccinated decimal(20) null,
	people_fully_vaccinated decimal(20) null,
	total_boosters decimal(20) null,
	new_vaccinations decimal(20) null,
	new_vaccinations_smoothed decimal(20) null,
	total_vaccinations_per_hundred decimal(20) null,
	people_vaccinated_per_hundred decimal(20) null,
	people_fully_vaccinated_per_hundred decimal(20) null,
	total_boosters_per_hundred decimal(20) null,
	new_vaccinations_smoothed_per_million decimal(20) null,
	new_people_vaccinated_smoothed decimal(20) null,
	new_people_vaccinated_smoothed_per_hundred decimal(20) null,
	excess_mortality_cumulative_absolute decimal(20) null,
	excess_mortality_cumulative decimal(20) null,
	excess_mortality decimal(20) null,
	excess_mortality_cumulative_per_million decimal(20) null,
	constraint fato_observacao_dim_data_id_fk
		foreign key (fk_data) references dim_data (id),
	constraint fato_observacao_dim_localizacao_id_fk
		foreign key (fk_localizacao) references dim_localizacao (id),
	constraint fato_observacao_dim_regiao_geografica_id_fk
		foreign key (fk_regiao_geografica) references dim_regiao_geografica (id),
	constraint fato_observacao_dim_testes_id_fk
		foreign key (fk_testes) references dim_testes (id)
);


