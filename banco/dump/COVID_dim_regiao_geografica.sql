insert into COVID.dim_regiao_geografica (id, cod_iso, ds_location, ds_continent)
values  (1, 'AGO', 'Angola', 'Africa'),
        (2, 'BDI', 'Burundi', 'Africa'),
        (3, 'BEN', 'Benin', 'Africa'),
        (4, 'BFA', 'Burkina Faso', 'Africa'),
        (5, 'BWA', 'Botswana', 'Africa'),
        (6, 'CAF', 'Central African Republic', 'Africa'),
        (7, 'CIV', 'Cote d''Ivoire', 'Africa'),
        (8, 'CMR', 'Cameroon', 'Africa'),
        (9, 'COD', 'Democratic Republic of Congo', 'Africa'),
        (10, 'COG', 'Congo', 'Africa'),
        (11, 'COM', 'Comoros', 'Africa'),
        (12, 'CPV', 'Cape Verde', 'Africa'),
        (13, 'DJI', 'Djibouti', 'Africa'),
        (14, 'DZA', 'Algeria', 'Africa'),
        (15, 'EGY', 'Egypt', 'Africa'),
        (16, 'ERI', 'Eritrea', 'Africa'),
        (17, 'ETH', 'Ethiopia', 'Africa'),
        (18, 'GAB', 'Gabon', 'Africa'),
        (19, 'GHA', 'Ghana', 'Africa'),
        (20, 'GIN', 'Guinea', 'Africa'),
        (21, 'GMB', 'Gambia', 'Africa'),
        (22, 'GNB', 'Guinea-Bissau', 'Africa'),
        (23, 'GNQ', 'Equatorial Guinea', 'Africa'),
        (24, 'KEN', 'Kenya', 'Africa'),
        (25, 'LBR', 'Liberia', 'Africa'),
        (26, 'LBY', 'Libya', 'Africa'),
        (27, 'LSO', 'Lesotho', 'Africa'),
        (28, 'MAR', 'Morocco', 'Africa'),
        (29, 'MDG', 'Madagascar', 'Africa'),
        (30, 'MLI', 'Mali', 'Africa'),
        (31, 'MOZ', 'Mozambique', 'Africa'),
        (32, 'MRT', 'Mauritania', 'Africa'),
        (33, 'MUS', 'Mauritius', 'Africa'),
        (34, 'MWI', 'Malawi', 'Africa'),
        (35, 'NAM', 'Namibia', 'Africa'),
        (36, 'NER', 'Niger', 'Africa'),
        (37, 'NGA', 'Nigeria', 'Africa'),
        (38, 'RWA', 'Rwanda', 'Africa'),
        (39, 'SDN', 'Sudan', 'Africa'),
        (40, 'SEN', 'Senegal', 'Africa'),
        (41, 'SHN', 'Saint Helena', 'Africa'),
        (42, 'SLE', 'Sierra Leone', 'Africa'),
        (43, 'SOM', 'Somalia', 'Africa'),
        (44, 'SSD', 'South Sudan', 'Africa'),
        (45, 'STP', 'Sao Tome and Principe', 'Africa'),
        (46, 'SWZ', 'Eswatini', 'Africa'),
        (47, 'SYC', 'Seychelles', 'Africa'),
        (48, 'TCD', 'Chad', 'Africa'),
        (49, 'TGO', 'Togo', 'Africa'),
        (50, 'TUN', 'Tunisia', 'Africa'),
        (51, 'TZA', 'Tanzania', 'Africa'),
        (52, 'UGA', 'Uganda', 'Africa'),
        (53, 'ZAF', 'South Africa', 'Africa'),
        (54, 'ZMB', 'Zambia', 'Africa'),
        (55, 'ZWE', 'Zimbabwe', 'Africa'),
        (56, 'AFG', 'Afghanistan', 'Asia'),
        (57, 'ARE', 'United Arab Emirates', 'Asia'),
        (58, 'ARM', 'Armenia', 'Asia'),
        (59, 'AZE', 'Azerbaijan', 'Asia'),
        (60, 'BGD', 'Bangladesh', 'Asia'),
        (61, 'BHR', 'Bahrain', 'Asia'),
        (62, 'BRN', 'Brunei', 'Asia'),
        (63, 'BTN', 'Bhutan', 'Asia'),
        (64, 'CHN', 'China', 'Asia'),
        (65, 'GEO', 'Georgia', 'Asia'),
        (66, 'HKG', 'Hong Kong', 'Asia'),
        (67, 'IDN', 'Indonesia', 'Asia'),
        (68, 'IND', 'India', 'Asia'),
        (69, 'IRN', 'Iran', 'Asia'),
        (70, 'IRQ', 'Iraq', 'Asia'),
        (71, 'ISR', 'Israel', 'Asia'),
        (72, 'JOR', 'Jordan', 'Asia'),
        (73, 'JPN', 'Japan', 'Asia'),
        (74, 'KAZ', 'Kazakhstan', 'Asia'),
        (75, 'KGZ', 'Kyrgyzstan', 'Asia'),
        (76, 'KHM', 'Cambodia', 'Asia'),
        (77, 'KOR', 'South Korea', 'Asia'),
        (78, 'KWT', 'Kuwait', 'Asia'),
        (79, 'LAO', 'Laos', 'Asia'),
        (80, 'LBN', 'Lebanon', 'Asia'),
        (81, 'LKA', 'Sri Lanka', 'Asia'),
        (82, 'MAC', 'Macao', 'Asia'),
        (83, 'MDV', 'Maldives', 'Asia'),
        (84, 'MMR', 'Myanmar', 'Asia'),
        (85, 'MNG', 'Mongolia', 'Asia'),
        (86, 'MYS', 'Malaysia', 'Asia'),
        (87, 'NPL', 'Nepal', 'Asia'),
        (88, 'OMN', 'Oman', 'Asia'),
        (89, 'OWID_CYN', 'Northern Cyprus', 'Asia'),
        (90, 'PAK', 'Pakistan', 'Asia'),
        (91, 'PHL', 'Philippines', 'Asia'),
        (92, 'PSE', 'Palestine', 'Asia'),
        (93, 'QAT', 'Qatar', 'Asia'),
        (94, 'SAU', 'Saudi Arabia', 'Asia'),
        (95, 'SGP', 'Singapore', 'Asia'),
        (96, 'SYR', 'Syria', 'Asia'),
        (97, 'THA', 'Thailand', 'Asia'),
        (98, 'TJK', 'Tajikistan', 'Asia'),
        (99, 'TKM', 'Turkmenistan', 'Asia'),
        (100, 'TLS', 'Timor', 'Asia'),
        (101, 'TUR', 'Turkey', 'Asia'),
        (102, 'TWN', 'Taiwan', 'Asia'),
        (103, 'UZB', 'Uzbekistan', 'Asia'),
        (104, 'VNM', 'Vietnam', 'Asia'),
        (105, 'YEM', 'Yemen', 'Asia'),
        (106, 'ALB', 'Albania', 'Europe'),
        (107, 'AND', 'Andorra', 'Europe'),
        (108, 'AUT', 'Austria', 'Europe'),
        (109, 'BEL', 'Belgium', 'Europe'),
        (110, 'BGR', 'Bulgaria', 'Europe'),
        (111, 'BIH', 'Bosnia and Herzegovina', 'Europe'),
        (112, 'BLR', 'Belarus', 'Europe'),
        (113, 'CHE', 'Switzerland', 'Europe'),
        (114, 'CYP', 'Cyprus', 'Europe'),
        (115, 'CZE', 'Czechia', 'Europe'),
        (116, 'DEU', 'Germany', 'Europe'),
        (117, 'DNK', 'Denmark', 'Europe'),
        (118, 'ESP', 'Spain', 'Europe'),
        (119, 'EST', 'Estonia', 'Europe'),
        (120, 'FIN', 'Finland', 'Europe'),
        (121, 'FRA', 'France', 'Europe'),
        (122, 'FRO', 'Faeroe Islands', 'Europe'),
        (123, 'GBR', 'United Kingdom', 'Europe'),
        (124, 'GGY', 'Guernsey', 'Europe'),
        (125, 'GIB', 'Gibraltar', 'Europe'),
        (126, 'GRC', 'Greece', 'Europe'),
        (127, 'HRV', 'Croatia', 'Europe'),
        (128, 'HUN', 'Hungary', 'Europe'),
        (129, 'IMN', 'Isle of Man', 'Europe'),
        (130, 'IRL', 'Ireland', 'Europe'),
        (131, 'ISL', 'Iceland', 'Europe'),
        (132, 'ITA', 'Italy', 'Europe'),
        (133, 'JEY', 'Jersey', 'Europe'),
        (134, 'LIE', 'Liechtenstein', 'Europe'),
        (135, 'LTU', 'Lithuania', 'Europe'),
        (136, 'LUX', 'Luxembourg', 'Europe'),
        (137, 'LVA', 'Latvia', 'Europe'),
        (138, 'MCO', 'Monaco', 'Europe'),
        (139, 'MDA', 'Moldova', 'Europe'),
        (140, 'MKD', 'North Macedonia', 'Europe'),
        (141, 'MLT', 'Malta', 'Europe'),
        (142, 'MNE', 'Montenegro', 'Europe'),
        (143, 'NLD', 'Netherlands', 'Europe'),
        (144, 'NOR', 'Norway', 'Europe'),
        (145, 'OWID_KOS', 'Kosovo', 'Europe'),
        (146, 'POL', 'Poland', 'Europe'),
        (147, 'PRT', 'Portugal', 'Europe'),
        (148, 'ROU', 'Romania', 'Europe'),
        (149, 'RUS', 'Russia', 'Europe'),
        (150, 'SMR', 'San Marino', 'Europe'),
        (151, 'SRB', 'Serbia', 'Europe'),
        (152, 'SVK', 'Slovakia', 'Europe'),
        (153, 'SVN', 'Slovenia', 'Europe'),
        (154, 'SWE', 'Sweden', 'Europe'),
        (155, 'UKR', 'Ukraine', 'Europe'),
        (156, 'VAT', 'Vatican', 'Europe'),
        (157, 'ABW', 'Aruba', 'North America'),
        (158, 'AIA', 'Anguilla', 'North America'),
        (159, 'ATG', 'Antigua and Barbuda', 'North America'),
        (160, 'BES', 'Bonaire Sint Eustatius and Saba', 'North America'),
        (161, 'BHS', 'Bahamas', 'North America'),
        (162, 'BLZ', 'Belize', 'North America'),
        (163, 'BMU', 'Bermuda', 'North America'),
        (164, 'BRB', 'Barbados', 'North America'),
        (165, 'CAN', 'Canada', 'North America'),
        (166, 'CRI', 'Costa Rica', 'North America'),
        (167, 'CUB', 'Cuba', 'North America'),
        (168, 'CUW', 'Curacao', 'North America'),
        (169, 'CYM', 'Cayman Islands', 'North America'),
        (170, 'DMA', 'Dominica', 'North America'),
        (171, 'DOM', 'Dominican Republic', 'North America'),
        (172, 'GRD', 'Grenada', 'North America'),
        (173, 'GRL', 'Greenland', 'North America'),
        (174, 'GTM', 'Guatemala', 'North America'),
        (175, 'HND', 'Honduras', 'North America'),
        (176, 'HTI', 'Haiti', 'North America'),
        (177, 'JAM', 'Jamaica', 'North America'),
        (178, 'KNA', 'Saint Kitts and Nevis', 'North America'),
        (179, 'LCA', 'Saint Lucia', 'North America'),
        (180, 'MEX', 'Mexico', 'North America'),
        (181, 'MSR', 'Montserrat', 'North America'),
        (182, 'NIC', 'Nicaragua', 'North America'),
        (183, 'PAN', 'Panama', 'North America'),
        (184, 'SLV', 'El Salvador', 'North America'),
        (185, 'SXM', 'Sint Maarten (Dutch part)', 'North America'),
        (186, 'TCA', 'Turks and Caicos Islands', 'North America'),
        (187, 'TTO', 'Trinidad and Tobago', 'North America'),
        (188, 'USA', 'United States', 'North America'),
        (189, 'VCT', 'Saint Vincent and the Grenadines', 'North America'),
        (190, 'VGB', 'British Virgin Islands', 'North America'),
        (191, 'AUS', 'Australia', 'Oceania'),
        (192, 'COK', 'Cook Islands', 'Oceania'),
        (193, 'FJI', 'Fiji', 'Oceania'),
        (194, 'FSM', 'Micronesia (country)', 'Oceania'),
        (195, 'KIR', 'Kiribati', 'Oceania'),
        (196, 'MHL', 'Marshall Islands', 'Oceania'),
        (197, 'NCL', 'New Caledonia', 'Oceania'),
        (198, 'NIU', 'Niue', 'Oceania'),
        (199, 'NRU', 'Nauru', 'Oceania'),
        (200, 'NZL', 'New Zealand', 'Oceania'),
        (201, 'PCN', 'Pitcairn', 'Oceania'),
        (202, 'PLW', 'Palau', 'Oceania'),
        (203, 'PNG', 'Papua New Guinea', 'Oceania'),
        (204, 'PYF', 'French Polynesia', 'Oceania'),
        (205, 'SLB', 'Solomon Islands', 'Oceania'),
        (206, 'TKL', 'Tokelau', 'Oceania'),
        (207, 'TON', 'Tonga', 'Oceania'),
        (208, 'TUV', 'Tuvalu', 'Oceania'),
        (209, 'VUT', 'Vanuatu', 'Oceania'),
        (210, 'WLF', 'Wallis and Futuna', 'Oceania'),
        (211, 'WSM', 'Samoa', 'Oceania'),
        (212, 'ARG', 'Argentina', 'South America'),
        (213, 'BOL', 'Bolivia', 'South America'),
        (214, 'BRA', 'Brazil', 'South America'),
        (215, 'CHL', 'Chile', 'South America'),
        (216, 'COL', 'Colombia', 'South America'),
        (217, 'ECU', 'Ecuador', 'South America'),
        (218, 'FLK', 'Falkland Islands', 'South America'),
        (219, 'GUY', 'Guyana', 'South America'),
        (220, 'PER', 'Peru', 'South America'),
        (221, 'PRY', 'Paraguay', 'South America'),
        (222, 'SUR', 'Suriname', 'South America'),
        (223, 'URY', 'Uruguay', 'South America'),
        (224, 'VEN', 'Venezuela', 'South America'),
        (225, 'OWID_AFR', 'Africa', null),
        (226, 'OWID_ASI', 'Asia', null),
        (227, 'OWID_EUN', 'European Union', null),
        (228, 'OWID_EUR', 'Europe', null),
        (229, 'OWID_HIC', 'High income', null),
        (230, 'OWID_INT', 'International', null),
        (231, 'OWID_LIC', 'Low income', null),
        (232, 'OWID_LMC', 'Lower middle income', null),
        (233, 'OWID_NAM', 'North America', null),
        (234, 'OWID_OCE', 'Oceania', null),
        (235, 'OWID_SAM', 'South America', null),
        (236, 'OWID_UMC', 'Upper middle income', null),
        (237, 'OWID_WRL', 'World', null);